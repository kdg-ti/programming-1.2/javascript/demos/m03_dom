const button = document.getElementById("btnHide");
const txt = document.getElementById("text");

function setButtonLabel() {
 // use value for input type=button
  //button.value = `Click to ${txt.hidden ? "unhide" : "hide"} the text`
  //use textContent for button element
  button.textContent = `Click to ${txt.hidden ? "unhide" : "hide"} the text`
}

function hideUnhide() {
  txt.hidden = !txt.hidden;
  setButtonLabel()
}

function addEventListeners() {
  button.addEventListener("click",hideUnhide)
}


function init(){
  setButtonLabel();
  addEventListeners();
}

init();