let wordList = document.querySelector("blockquote").textContent.split(" ");
const result = document.getElementById("result");
const resultFluid = document.getElementById("result-fluid");
const resultHtml = document.getElementById("result-html");
const resultHtmlFluid = document.getElementById("result-html-fluid");

init();

function createElementWithText(type, word) {
  const element = document.createElement(type);
  element.append(document.createTextNode(word));
  return element;
}

function deleteEventListener( word) {
    wordList.splice(wordList.indexOf(word),1);
    result.replaceChildren(createList(wordList));
}

function createList(wordlist) {
  const list = document.createElement("ul");
  for (const word of wordlist){
    const item = createElementWithText("li",word);
    const button = createElementWithText("button", "delete");
    // with a lambda
    // button.addEventListener("click",event => {
    //   wordList.splice(wordList.indexOf(word),1);
    //   result.replaceChildren(createList(wordList));
    // })
    // with a separate function
    button.addEventListener("click" , event => deleteEventListener(word));
    item.append(button);
    list.append(item);
  }
  return list;
}



function createFluidList(wordlist) {
  const list = document.createElement("ol");
  list.append(...wordlist.map(word => createElementWithText("li",word)));
  return list;
}

function createHtmlList(wordlist) {
  let list = "<ul>";
  for (const word of wordlist){
    list += `<li>${word.toUpperCase()}</li>`;
  }
  return list + "</ul>";
}

function createHtmlFluidList(wordlist) {
  return wordlist.reduce(
    (acc,word) => acc + `<li>${word.toUpperCase()}</li>`,
    "<ol>")
  + "</ol>";
}

function init(){
  result.replaceChildren(createList(wordList));
  resultFluid.replaceChildren(createFluidList(wordList));
  resultHtml.innerHTML = createHtmlList(wordList);
  resultHtmlFluid.innerHTML = createHtmlFluidList(wordList);
}