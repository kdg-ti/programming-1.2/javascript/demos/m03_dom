const indentString="--";
function printElement(level,node) {
  let output = indentString.repeat(level)+node.nodeName;
  if(node.nodeType !== Node.ELEMENT_NODE){
    output+= " " + node.nodeValue
  }
  console.log(output);
if(node.nodeName !== "SCRIPT") {
  // for(const child of node.children){
  for (const child of node.childNodes) {
    printElement(level + 1, child);
  }
}
}

function init() {
  const body = document.documentElement;
  printElement(0,body)
}

init();
