const wordList = document.querySelector("blockquote").textContent.split(" ");
const result = document.getElementById("result");
const resultFluid = document.getElementById("result-fluid");
const resultHtml = document.getElementById("result-html");
const resultHtmlFluid = document.getElementById("result-html-fluid");

init();

function createElementWithText(type, word) {
  const element = document.createElement(type);
  element.append(document.createTextNode(word));
  return element;
}

function createList(wordlist) {
  const list = document.createElement("ul");
  for (const word of wordlist){
    list.append(createElementWithText("li",word));
  }
  return list;
}

function createFluidList(wordlist) {
  const list = document.createElement("ol");
  list.append(...wordlist.map(word => createElementWithText("li",word)));
  return list;
}

function createHtmlList(wordlist) {
  let list = "<ul>";
  for (const word of wordlist){
    list += `<li>${word.toUpperCase()}</li>`;
  }
  return list + "</ul>";
}

function createHtmlFluidList(wordlist) {
  return wordlist.reduce(
    (acc,word) => acc + `<li>${word.toUpperCase()}</li>`,
    "<ol>")
  + "</ol>";
}

function init(){
  result.append(createList(wordList));
  resultFluid.append(createFluidList(wordList));
  resultHtml.innerHTML = createHtmlList(wordList);
  resultHtmlFluid.innerHTML = createHtmlFluidList(wordList);
}